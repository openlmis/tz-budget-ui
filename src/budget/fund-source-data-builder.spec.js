/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('budget')
        .factory('SourceOfFundDataBuilder', SourceOfFundDataBuilder);

    SourceOfFundDataBuilder.$inject = ['SourceOfFund'];

    function SourceOfFundDataBuilder(SourceOfFund) {

        SourceOfFundDataBuilder.prototype.build = build;
        SourceOfFundDataBuilder.prototype.withId = withId;
        SourceOfFundDataBuilder.prototype.withCode = withCode;
        SourceOfFundDataBuilder.prototype.withName = withName;
        SourceOfFundDataBuilder.prototype.withDisplayOrder = withDisplayOrder;

        return SourceOfFundDataBuilder;

        function SourceOfFundDataBuilder() {
            this.id = '97c3acd7-4d13-4433-b814-b9226b450c08';
            this.code = 'some code';
            this.name = 'some name';
            this.displayOrder = 2;
        }

        function withId(id) {
            this.id = id;
            return this;
        }

        function withCode(code) {
            this.code = code;
            return this;
        }

        function withName(name) {
            this.name = name;
            return this;
        }

        function withDisplayOrder(displayOrder) {
            this.displayOrder = displayOrder;
            return this;
        }

        function build() {
            return new SourceOfFund(
                this.id,
                this.code,
                this.name,
                this.displayOrder
            );
        }

    }

})();