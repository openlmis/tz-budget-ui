/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('budgetService', function() {

    var $rootScope, $httpBackend, budgetUrlFactory, budgetService, budgets, BudgetDataBuilder;

    beforeEach(function() {
        module('budget', function($provide) {
            budgetUrlFactory = jasmine.createSpy().andCallFake(function(value) {
                return value;
            });
            $provide.factory('budgetUrlFactory', function() {
                return budgetUrlFactory;
            });
        });

        inject(function($injector) {
            $httpBackend = $injector.get('$httpBackend');
            $rootScope = $injector.get('$rootScope');
            budgetUrlFactory = $injector.get('budgetUrlFactory');
            budgetService = $injector.get('budgetService');
            BudgetDataBuilder = $injector.get('BudgetDataBuilder');
        });

        budgets = [
            new BudgetDataBuilder().withId('1')
                .build(),
            new BudgetDataBuilder().withId('2')
                .build()
        ];

    });

    describe('get', function() {

        beforeEach(function() {
            $httpBackend.when('GET', budgetUrlFactory('/api/budgets/' + budgets[0].id))
                .respond(200, budgets[0]);
        });

        it('should return promise', function() {
            var result = budgetService.get(budgets[0].id);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to budget', function() {
            var result;

            budgetService.get(budgets[0].id).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(budgets[0]));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(budgetUrlFactory('/api/budgets/' + budgets[0].id));

            budgetService.get(budgets[0].id);
            $httpBackend.flush();
        });
    });

    describe('getAll', function() {

        var response;

        beforeEach(function() {
            response = {
                content: budgets,
                last: true,
                totalElements: 0,
                totalPages: 0,
                sort: null,
                first: true,
                numberOfElements: 0,
                size: 2000,
                number: 0
            };
        });

        it('should return promise resolving to a list of budgets', function() {
            var result;

            $httpBackend.when('GET', budgetUrlFactory('/api/budgets'))
                .respond(200, response);

            budgetService.getAll().then(function(response) {
                result = response;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(response));
        });

    });

    describe('upload', function() {

        var file;

        beforeEach(function() {
            file = 'file-content';
            $httpBackend.when('POST', budgetUrlFactory('/api/budgets/upload?format=csv')).respond(200, {
                content: file
            });
        });

        it('should return promise', function() {
            var result = budgetService.upload(file);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to budgets', function() {
            var result;

            budgetService.upload(file).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result.content)).toEqual(angular.toJson(file));
        });

        it('should make a proper request', function() {
            $httpBackend.expectPOST(budgetUrlFactory('/api/budgets/upload?format=csv'));

            budgetService.upload(file);
            $httpBackend.flush();
        });
    });

    describe('getDownloadUrl', function() {

        it('should expose getDownloadUrl method', function() {
            expect(angular.isFunction(budgetService.getDownloadUrl)).toBe(true);
        });

        it('should call budgetUrlFactory', function() {
            var result = budgetService.getDownloadUrl();

            expect(result).toEqual('/api/budgets/upload?format=csv');
            expect(budgetUrlFactory).toHaveBeenCalledWith('/api/budgets/upload?format=csv');
        });
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });
});
