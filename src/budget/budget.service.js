/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name budget.budgetService
     *
     * @description
     * Responsible for retrieving BUDGET from the server.
     */
    angular
        .module('budget')
        .factory('budgetService', service);

    service.$inject = ['budgetUrlFactory', '$resource', 'FormData'];

    function service(budgetUrlFactory, $resource, FormData) {

        var resource = $resource(budgetUrlFactory('/api/budgets/:id'), {}, {
            upload: {
                url: budgetUrlFactory('/api/budgets/upload?format=csv'),
                method: 'POST',
                headers: {
                    'Content-Type': undefined
                }
            },
            getAll: {
                url: budgetUrlFactory('/api/budgets'),
                method: 'GET'
            },
            getByFacilityId: {
                url: budgetUrlFactory('/api/budgets/:facilityId/budgets'),
                method: 'GET'
            }
        });

        return {
            get: get,
            upload: upload,
            getAll: getAll,
            getDownloadUrl: getDownloadUrl,
            getByFacilityId: getByFacilityId
        };

        /**
         * @ngdoc method
         * @methodOf budget.budgetService
         * @name get
         *
         * @description
         * Gets Source of fund by id.
         *
         * @param  {String}  id budget UUID
         * @return {Promise}    Budget info
         */
        function get(id) {
            return resource.get({
                id: id
            }).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf budget.budgetService
         * @name getAll
         *
         * @description
         * Gets all Budgets.
         *
         * @return {Promise} Array of all Budget
         */
        function getAll() {
            return resource.getAll().$promise;
        }

        /**
         * @ngdoc method
         * @methodOf budget.budgetService
         * @name upload
         *
         * @description
         * Uploads Budget with csv file.
         *
         * @param  {Object}  file the csv file that will be uploaded
         * @return {Promise}      the uploaded Budget
         */
        function upload(file) {
            var formData = new FormData();
            formData.append('file', file);

            return resource.upload(formData).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf budget.budgetService
         * @name getDownloadUrl
         *
         * @description
         * Returns URL for downloading budget in csv format file.
         *
         * @return {String} the URL for downloading budgets
         */
        function getDownloadUrl() {
            return budgetUrlFactory('/api/budgets/upload?format=csv');
        }

        /**
         * @ngdoc method
         * @methodOf budget.budgetService
         * @name get
         *
         * @description
         * Gets Budgets by facility id.
         *
         * @param  {String}  facilityId facility UUID
         * @return {Promise}    Budget info
         */
        function getByFacilityId(facilityId) {
            return resource.get({
                facilityId: facilityId
            }).$promise;
        }

    }
})();
