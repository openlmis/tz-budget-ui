/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name budget.sourceOfFundService
     *
     * @description
     * Responsible for retrieving BUDGET from the server.
     */
    angular
        .module('budget')
        .factory('sourceOfFundService', service);

    service.$inject = ['budgetUrlFactory', '$resource', 'FormData'];

    function service(budgetUrlFactory, $resource, FormData) {

        var resource = $resource(budgetUrlFactory('/api/sourceOfFunds/:id'), {}, {
            upload: {
                url: budgetUrlFactory('/api/sourceOfFunds/upload?format=csv'),
                method: 'POST',
                headers: {
                    'Content-Type': undefined
                }
            },
            getAll: {
                url: budgetUrlFactory('/api/sourceOfFunds'),
                method: 'GET'
            },
            update: {
                url: budgetUrlFactory('/api/sourceOfFunds/:id'),
                method: 'PUT'
            }
        });

        return {
            get: get,
            upload: upload,
            getAll: getAll,
            getDownloadUrl: getDownloadUrl,
            update: update,
            create: create
        };

        /**
         * @ngdoc method
         * @methodOf budget.sourceOfFundService
         * @name get
         *
         * @description
         * Gets Source of fund by id.
         *
         * @param  {String}  id Source of fund UUID
         * @return {Promise}    Source of Fund info
         */
        function get(id) {
            return resource.get({
                id: id
            }).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf budget.sourceOfFundService
         * @name getAll
         *
         * @description
         * Gets all Source Of Funds.
         *
         * @return {Promise} Array of all Source of funds
         */
        function getAll() {
            return resource.getAll().$promise;
        }

        /**
         * @ngdoc method
         * @methodOf budget.sourceOfFundService
         * @name upload
         *
         * @description
         * Uploads Source of Fund with csv file.
         *
         * @param  {Object}  file the csv file that will be uploaded
         * @return {Promise}      the uploaded Source of fund
         */
        function upload(file) {
            var formData = new FormData();
            formData.append('file', file);

            return resource.upload(formData).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf budget.sourceOfFundService
         * @name getDownloadUrl
         *
         * @description
         * Returns URL for downloading source of fund in csv format file.
         *
         * @return {String} the URL for downloading source of funds
         */
        function getDownloadUrl() {
            return budgetUrlFactory('/api/sourceOfFunds/upload?format=csv');
        }

        /**
         * @ngdoc method
         * @methodOf budget.sourceOfFundService
         * @name update
         *
         * @description
         * Updates existing Source Of Fund.
         *
         * @param  {Object}  sourceOfFund source of fund that will be saved
         * @return {Promise}              updated source of fund
         */
        function update(sourceOfFund) {
            return resource.update({
                id: sourceOfFund.id
            }, sourceOfFund).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf budget.sourceOfFundService
         * @name create
         *
         * @description
         * Creates new Source Of Fund.
         *
         * @param  {Object}  sourceOfFund source of fund that will be created
         * @return {Promise} created source of fund
         */
        function create(sourceOfFund) {
            return resource.save(sourceOfFund).$promise;
        }

    }
})();
