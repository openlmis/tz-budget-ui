/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name budget.Budget
     *
     * @description
     * Represents a single budget.
     */
    angular
        .module('budget')
        .factory('Budget', Budget);

    function Budget() {

        return Budget;

        /**
         * @ngdoc method
         * @methodOf budget.Budget
         * @name Budget
         *
         * @description
         * Creates a new instance of the Budget class.
         *
         * @param  {String}  id              the UUID of the budget to be updated
         * @param  {String}  facilityId      the UUID of the budget to be created
         * @param  {String}  programId       the UUID of the budget to be created
         * @param  {String}  sourceOfFundId  the UUID of the budget to be created
         * @param  {Number}  budgetAmount    the amount of the budget amount to be created
         * @param  {String}  note            the note or comment of the budget to be created
         * @param  {Date}    lastUpdatedDate the last updated date of the budget to be created
         * @return {Object}                  the budget object
         */
        function Budget(id, facilityId, programId, sourceOfFundId, budgetAmount,
                        note, lastUpdatedDate) {
            this.id = id;
            this.facilityId = facilityId;
            this.programId = programId;
            this.sourceOfFundId = sourceOfFundId;
            this.budgetAmount  = budgetAmount;
            this.note = note;
            this.lastUpdatedDate = lastUpdatedDate;

        }

    }

})();
