/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name budget.SourceOfFund
     *
     * @description
     * Represents a single SourceOfFund.
     */
    angular
        .module('budget')
        .factory('SourceOfFund', SourceOfFund);

    function SourceOfFund() {

        return SourceOfFund;

        /**
         * @ngdoc method
         * @methodOf budget.SourceOfFund
         * @name SourceOfFund
         *
         * @description
         * Creates a new instance of the SourceOfFund class.
         *
         * @param  {String}  id              the UUID of the source of fund to be updated
         * @param  {String}  code      the code of the source of fund to be created
         * @param  {String}  name       the name of the source of fund to be created
         * @param  {Number}  displayOrder  the display order of the source of fund to be created
         * @return {Object}                  the source of fund object
         */
        function SourceOfFund(id, code, name, displayOrder) {
            this.id = id;
            this.code = code;
            this.name = name;
            this.displayOrder = displayOrder;
        }

    }

})();
