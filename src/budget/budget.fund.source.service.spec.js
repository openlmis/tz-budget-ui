/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('sourceOfFundService', function() {

    var $rootScope, $httpBackend, budgetUrlFactory, sourceOfFundService,  sourceOfFunds, SourceOfFundDataBuilder;

    beforeEach(function() {
        module('budget', function($provide) {
            budgetUrlFactory = jasmine.createSpy().andCallFake(function(value) {
                return value;
            });
            $provide.factory('budgetUrlFactory', function() {
                return budgetUrlFactory;
            });
        });

        inject(function($injector) {
            $httpBackend = $injector.get('$httpBackend');
            $rootScope = $injector.get('$rootScope');
            budgetUrlFactory = $injector.get('budgetUrlFactory');
            sourceOfFundService = $injector.get('sourceOfFundService');
            SourceOfFundDataBuilder = $injector.get('SourceOfFundDataBuilder');
        });

        sourceOfFunds = [
            new SourceOfFundDataBuilder().withId('1')
                .build(),
            new SourceOfFundDataBuilder().withId('2')
                .build()
        ];
    });

    describe('get', function() {

        beforeEach(function() {
            $httpBackend.when('GET', budgetUrlFactory('/api/sourceOfFunds/' + sourceOfFunds[0].id))
                .respond(200, sourceOfFunds[0]);
        });

        it('should return promise', function() {
            var result = sourceOfFundService.get(sourceOfFunds[0].id);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to source of fund', function() {
            var result;

            sourceOfFundService.get(sourceOfFunds[0].id).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(sourceOfFunds[0]));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(budgetUrlFactory('/api/sourceOfFunds/' + sourceOfFunds[0].id));

            sourceOfFundService.get(sourceOfFunds[0].id);
            $httpBackend.flush();
        });
    });

    describe('getAll', function() {

        var response;

        beforeEach(function() {
            response = {
                content: sourceOfFunds,
                last: true,
                totalElements: 0,
                totalPages: 0,
                sort: null,
                first: true,
                numberOfElements: 0,
                size: 2000,
                number: 0
            };
        });

        it('should return promise resolving to a list of source of funds', function() {
            var result;

            $httpBackend.when('GET', budgetUrlFactory('/api/sourceOfFunds'))
                .respond(200, response);

            sourceOfFundService.getAll().then(function(response) {
                result = response;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(response));
        });

    });

    describe('upload', function() {

        var file;

        beforeEach(function() {
            file = 'file-content';
            $httpBackend.when('POST', budgetUrlFactory('/api/sourceOfFunds/upload?format=csv')).respond(200, {
                content: file
            });
        });

        it('should return promise', function() {
            var result = sourceOfFundService.upload(file);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to source of funds', function() {
            var result;

            sourceOfFundService.upload(file).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result.content)).toEqual(angular.toJson(file));
        });

        it('should make a proper request', function() {
            $httpBackend.expectPOST(budgetUrlFactory('/api/sourceOfFunds/upload?format=csv'));

            sourceOfFundService.upload(file);
            $httpBackend.flush();
        });
    });

    describe('getDownloadUrl', function() {

        it('should expose getDownloadUrl method', function() {
            expect(angular.isFunction(sourceOfFundService.getDownloadUrl)).toBe(true);
        });

        it('should call budgetUrlFactory', function() {
            var result = sourceOfFundService.getDownloadUrl();

            expect(result).toEqual('/api/sourceOfFunds/upload?format=csv');
            expect(budgetUrlFactory).toHaveBeenCalledWith('/api/sourceOfFunds/upload?format=csv');
        });
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });
});
