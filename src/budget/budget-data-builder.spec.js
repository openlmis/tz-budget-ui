/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('budget')
        .factory('BudgetDataBuilder', BudgetDataBuilder);

    BudgetDataBuilder.$inject = ['Budget'];

    function BudgetDataBuilder(Budget) {

        BudgetDataBuilder.prototype.build = build;
        BudgetDataBuilder.prototype.withId = withId;
        BudgetDataBuilder.prototype.withFacilityId = withFacilityId;
        BudgetDataBuilder.prototype.withSourceOfFundId = withSourceOfFundId;
        BudgetDataBuilder.prototype.withProgramId = withProgramId;
        BudgetDataBuilder.prototype.withNote = withNote;
        BudgetDataBuilder.prototype.withLastUpdatedDate = withLastUpdatedDate;
        BudgetDataBuilder.prototype.withBudgetAmount = withBudgetAmount;

        return BudgetDataBuilder;

        function BudgetDataBuilder() {
            this.id = 'df135ec9-8dda-4f6b-aac0-4ae69c684990';
            this.facilityId = '3389c9c4-6862-4133-8277-1dd2f36b3cde';
            this.sourceOfFundId = 'b77b2189-e9ce-401f-9c3b-9513c57513e2';
            this.programId = 'a99f179f-4e37-4877-bb12-18a3b36d92e0';
            this.note = 'Some notes';
            this.budgetAmount = 50000;
            this.lastUpdatedDate = '2020-11-30';
        }

        function withId(id) {
            this.id = id;
            return this;
        }

        function withFacilityId(facilityId) {
            this.facilityId = facilityId;
            return this;
        }

        function withProgramId(programId) {
            this.programId = programId;
            return this;
        }

        function withSourceOfFundId(sourceOfFundId) {
            this.sourceOfFundId = sourceOfFundId;
            return this;
        }

        function withNote(note) {
            this.note = note;
            return this;
        }

        function withBudgetAmount(budgetAmount) {
            this.budgetAmount = budgetAmount;
            return this;
        }

        function withLastUpdatedDate(lastUpdatedDate) {
            this.lastUpdatedDate = lastUpdatedDate;
            return this;
        }

        function build() {
            return new Budget(
                this.id,
                this.facilityId,
                this.programId,
                this.sourceOfFundId,
                this.note,
                this.budgetAmount,
                this.lastUpdatedDate
            );
        }

    }

})();