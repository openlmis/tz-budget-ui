/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name admin-fund-source-list.controller:FundSourceListController
     *
     * @description
     * Controller for source of funds list screen.
     */
    angular
        .module('admin-fund-source-list')
        .controller('FundSourceListController', controller);

    controller.$inject = ['sourceOfFunds'];

    function controller(sourceOfFunds) {

        var vm = this;

        vm.$onInit = onInit;

        /**
         * @ngdoc property
         * @propertyOf admin-fund-source-list.controller:FundSourceListController
         * @name sourceOfFunds
         * @type {Array}
         *
         * @description
         * Contains page of source of funds.
         */
        vm.sourceOfFunds = undefined;

        /**
         * @ngdoc method
         * @propertyOf admin-fund-source-list.controller:FundSourceListController
         * @name $onInit
         *
         * @description
         * Method that is executed on initiating FundSourceListController.
         */
        function onInit() {
            vm.sourceOfFunds = sourceOfFunds;
        }
    }
})();
