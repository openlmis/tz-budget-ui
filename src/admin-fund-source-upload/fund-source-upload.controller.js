/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name admin-fund-source-upload.controller:SourceOfFundUploadController
     *
     * @description
     * Controller for uploading Source of funds.
     */
    angular
        .module('admin-fund-source-upload')
        .controller('SourceOfFundUploadController', controller);

    controller.$inject = [
        '$state', 'sourceOfFundService', 'notificationService', 'messageService', 'loadingModalService'
    ];

    function controller($state, sourceOfFundService, notificationService, messageService, loadingModalService) {

        var vm = this;

        vm.getExportUrl = getExportUrl;
        vm.upload = upload;

        /**
         * @ngdoc property
         * @propertyOf admin-fund-source-upload.controller:SourceOfFundUploadController
         * @name file
         * @type {Object}
         *
         * @description
         * Holds csv file.
         */
        vm.file = undefined;

        /**
         * @ngdoc property
         * @propertyOf admin-fund-source-upload.controller:SourceOfFundUploadController
         * @name invalidMessage
         * @type {String}
         *
         * @description
         * Holds form error message.
         */
        vm.invalidMessage = undefined;

        /**
         * @ngdoc method
         * @methodOf admin-fund-source-upload.controller:SourceOfFundUploadController
         * @name upload
         *
         * @description
         * Uploads csv file with source of fund to the server.
         */
        function upload() {
            vm.invalidMessage = undefined;

            if (vm.file) {
                var loadingPromise = loadingModalService.open();
                sourceOfFundService.upload(vm.file).then(function(data) {
                    var message = messageService.get(
                        'adminSourceOfFundUpload.uploadSuccess',
                        {
                            amount: data.amount
                        }
                    );
                    loadingPromise.then(function() {
                        notificationService.success(message);
                    });

                    $state.reload();
                }, function(error) {
                    notificationService.error('adminSourceOfFundUpload.uploadFailed');
                    vm.invalidMessage = error ? error.data.message : undefined;
                    vm.file = undefined;
                    loadingModalService.close();
                });
            } else {
                notificationService.error('adminSourceOfFundUpload.fileIsNotSelected');
            }
        }

        /**
         * @ngdoc method
         * @methodOf admin-fund-source-upload.controller:SourceOfFundUploadController
         * @name getExportUrl
         *
         * @description
         * Returns url for downloading csv file with all sources of fund.
         */
        function getExportUrl() {
            return sourceOfFundService.getDownloadUrl();
        }
    }

})();
