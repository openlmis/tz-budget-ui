/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
    * @ngdoc controller
    * @name admin-fund-source-edit.controller:FundSourceEditController
    *
    * @description
    * Provides methods for Edit Source Of Fund modal. Allows returning to previous state and editing Source Of Fund.
    */
    angular
        .module('admin-fund-source-edit')
        .controller('FundSourceEditController', FundSourceEditController);

    FundSourceEditController.$inject = [
        'sourceOfFund', 'confirmService', 'sourceOfFundService', 'stateTrackerService',
        '$state', 'loadingModalService', 'notificationService'
    ];

    function FundSourceEditController(sourceOfFund, confirmService, sourceOfFundService,
                                      stateTrackerService, $state, loadingModalService, notificationService) {
        var vm = this;

        vm.save = save;
        vm.goToPreviousState = stateTrackerService.goToPreviousState;
        vm.$onInit = onInit;

        /**
         * @ngdoc property
         * @methodOf admin-fund-source-edit.controller:FundSourceEditController
         * @name sourceOfFund
         * @type {Object}
         *
         * @description
         * Current Source Of Fund.
         */
        vm.sourceOfFund = undefined;

        /**
         * @ngdoc property
         * @methodOf admin-fund-source-edit.controller:FundSourceEditController
         * @name editMode
         * @type {boolean}
         *
         * @description
         * Indicates if fund source is already created.
         */
        vm.editMode = undefined;

        /**
         * @ngdoc method
         * @methodOf admin-fund-source-edit.controller:FundSourceEditController
         * @name $onInit
         *
         * @description
         * Method that is executed on initiating FundSourceEditController.
         */
        function onInit() {
            vm.sourceOfFund = sourceOfFund ? sourceOfFund : {
                displayOrder: 1,
                active: true
            };
            vm.editMode = !!sourceOfFund;
        }

        /**
         * @ngdoc method
         * @methodOf admin-fund-source-edit.controller:FundSourceEditController
         * @name save
         *
         * @description
         * Saves the Source of Fund after confirm.
         */
        function save() {
            confirmService.confirm(
                vm.editMode ? 'adminFundSourceEdit.save.question' : 'adminFundSourceEdit.create.question',
                vm.editMode ? 'adminFundSourceEdit.save' : 'adminFundSourceEdit.create'
            ).then(function() {
                loadingModalService.open();
                getSavePromise()
                    .then(function() {
                        notificationService.success(vm.editMode ?
                            'adminFundSourceEdit.save.success' : 'adminFundSourceEdit.create.success');
                        stateTrackerService.goToPreviousState();
                    })
                    .catch(function() {
                        loadingModalService.close();
                        notificationService.error(
                            vm.editMode ? 'adminFundSourceEdit.save.failure' : 'adminFundSourceEdit.create.failure'
                        );
                    });
            });
        }

        function getSavePromise() {
            return vm.editMode ?
                sourceOfFundService.update(vm.sourceOfFund) :
                sourceOfFundService.create(vm.sourceOfFund);
        }
    }
})();
