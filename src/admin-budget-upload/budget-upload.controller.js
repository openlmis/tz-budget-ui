/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name admin-budget-upload.controller:BudgetUploadController
     *
     * @description
     * Controller for uploading budgets.
     */
    angular
        .module('admin-budget-upload')
        .controller('BudgetUploadController', controller);

    controller.$inject = [
        '$state', 'budgetService', 'notificationService', 'messageService', 'loadingModalService'
    ];

    function controller($state, budgetService, notificationService, messageService, loadingModalService) {

        var vm = this;

        vm.getExportUrl = getExportUrl;
        vm.upload = upload;

        /**
         * @ngdoc property
         * @propertyOf admin-budget-upload.controller:BudgetUploadController
         * @name file
         * @type {Object}
         *
         * @description
         * Holds csv file.
         */
        vm.file = undefined;

        /**
         * @ngdoc property
         * @propertyOf admin-budget-upload.controller:BudgetUploadController
         * @name invalidMessage
         * @type {String}
         *
         * @description
         * Holds form error message.
         */
        vm.invalidMessage = undefined;

        /**
         * @ngdoc method
         * @methodOf admin-budget-upload.controller:BudgetUploadController
         * @name upload
         *
         * @description
         * Uploads csv file with budget to the server.
         */
        function upload() {
            vm.invalidMessage = undefined;

            if (vm.file) {
                var loadingPromise = loadingModalService.open();
                budgetService.upload(vm.file).then(function(data) {
                    var message = messageService.get(
                        'adminBudgetUpload.uploadSuccess',
                        {
                            amount: data.amount
                        }
                    );
                    loadingPromise.then(function() {
                        notificationService.success(message);
                    });

                    $state.reload();
                }, function(error) {
                    notificationService.error('adminBudgetUpload.uploadFailed');
                    vm.invalidMessage = error ? error.data.message : undefined;
                    vm.file = undefined;
                    loadingModalService.close();
                });
            } else {
                notificationService.error('adminBudgetUpload.fileIsNotSelected');
            }
        }

        /**
         * @ngdoc method
         * @methodOf admin-budget-upload.controller:BudgetUploadController
         * @name getExportUrl
         *
         * @description
         * Returns url for downloading csv file with all budget.
         */
        function getExportUrl() {
            return budgetService.getDownloadUrl();
        }
    }

})();
